from django.conf.urls import include, url


from .views import (
    NewsList,
)

urlpatterns = [
    url(r'^$', NewsList.as_view(), name='news_page'),
]
