from django.core.mail import EmailMessage
from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.views import generic

from index.models import Site, Order


class IndexView(generic.TemplateView):
    template_name = 'index/news.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data()
        site = Site.objects.all().last()
        context['site'] = site
        context['order_status'] = self.request.GET.get('order')
        return context


class OrderView(generic.View):

    def post(self, request, *args, **kwargs):
        try:
            # print('checkpoint 1')
            user_name = request.POST.get('name')
            # print(user_name)
            user_phone = request.POST.get('phone')
            # print(user_phone)
            comment = request.POST.get('text')
            # print(comment)
            if comment and user_phone and user_name:
                Order.objects.get_or_create(user_name=user_name, user_phone=user_phone, user_comment=comment)
                body = 'Имя: %s\nТелефон: %s\n\nВопрос: %s' % (user_name, user_phone, comment)
                email = EmailMessage(
                    'Новый заказ с формы на сайте',
                    body,
                    'info@urist-mos.ru',
                    ['info@urist-mos.ru', ],
                    [],
                    reply_to=['info@urist-mos.ru'],
                    # headers={'Message-ID': 'foo'},
                )

                email.send()

                return redirect('/?order=ok')
            else:
                raise ValueError
        except ValueError:
            return redirect('/?order=error')
