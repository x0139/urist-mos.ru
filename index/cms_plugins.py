from cms.plugin_base import CMSPluginBase
from cms.plugin_pool import plugin_pool
from cms.models.pluginmodel import CMSPlugin
from django.utils.translation import ugettext_lazy as _

from index.models import FooterPlugin, HeaderPlugin


@plugin_pool.register_plugin
class HeaderPlugin(CMSPluginBase):
    model = HeaderPlugin
    render_template = "index/includes/header.html"
    name = 'Шапка'
    module = 'Header/Footer'

    def render(self, context, instance, placeholder):
        context.update({
            'instance': instance,
        })
        return context


@plugin_pool.register_plugin
class FooterPlugin(CMSPluginBase):
    model = FooterPlugin
    render_template = "index/includes/footer.html"
    name = 'Подвал'
    module = 'Header/Footer'

    def render(self, context, instance, placeholder):
        context.update({
            'instance': instance,
        })
        return context
