from cms.models import CMSPlugin
from django.db import models

# Create your models here.
from djangocms_text_ckeditor.fields import HTMLField
from filer.fields.file import FilerFileField
from filer.fields.image import FilerImageField


class Site(models.Model):
    name = models.CharField(default='Юридическая помощь', max_length=50)
    title = models.CharField(default='urist-mos: Юридическая помощь', max_length=100)
    url = models.CharField(default='http://urist-mos.ru/', max_length=20)
    description = models.TextField(default='Юридические услуги в Москве', max_length=400)
    keywords = models.TextField(
        default='Гражданские дела, семейные дела, защита прав потребителей, страховые дела, помощт при ДТП, ' \
                'помощь дольщимкам, трудовые дела', max_length=300)

    def __str__(self):
        return self.name


class Order(models.Model):
    created_at = models.DateTimeField(auto_now=True)
    user_name = models.CharField(max_length=50, default=None, null=False)
    user_phone = models.CharField(max_length=20, default=None, null=False)
    user_comment = models.TextField(max_length=500, default=None, null=False)

    def __str__(self):
        return "%s, %s" % (self.user_name, self.created_at)


class Notification(models.Model):
    ok_text = models.TextField(max_length=500, default=None, null=False)
    error_text = models.TextField(max_length=500, default=None, null=False)


class Phone(models.Model):
    phone_number = models.CharField(max_length=20, blank=True, null=True, verbose_name='Номер телефона')

    def __str__(self):
        return self.phone_number


class HeaderPlugin(CMSPlugin):
    head_text = models.CharField(max_length=500, verbose_name='Заголовок')
    main_text = HTMLField(max_length=2000, null=True, blank=True, verbose_name='Основной текст')
    phone_text = models.CharField(max_length=500, verbose_name='Заголовок справа', default='Телефон в Москве',
                                  blank=True, null=True)
    phones = models.ManyToManyField(Phone, verbose_name='Телефоны', blank=True, null=True)

    def copy_relations(self, oldinstance):
        self.phones = oldinstance.phones.all()


class FooterImage(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True, verbose_name='Название')
    image = FilerImageField(null=True, blank=True, verbose_name='Картинка')
    url = models.CharField(max_length=255, null=True, blank=True, verbose_name='URL')

    def __str__(self):
        return self.name or 'Картинка'


class FooterPlugin(CMSPlugin):
    footer_images = models.ManyToManyField(FooterImage, verbose_name='Картинки')
    text = HTMLField(max_length=2000, null=True, blank=True, verbose_name='Текст')

    def copy_relations(self, oldinstance):
        self.footer_images = oldinstance.footer_images.all()
