from django.contrib import admin

from index.models import Site, Order, FooterImage, Phone


class SiteAdmin(admin.ModelAdmin):
    list_display = ('name', 'title', 'keywords', 'description', 'url')


class OrderAdmin(admin.ModelAdmin):
    list_display = ('created_at', 'user_name', 'user_phone', 'user_comment')

admin.site.register(Site, SiteAdmin)
admin.site.register(Order, OrderAdmin)
admin.site.register(FooterImage)
admin.site.register(Phone)